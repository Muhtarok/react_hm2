import PropTypes from 'prop-types'

import React from 'react';
// import ShoppingCartIcon from '../../assets/';
import styles from "../Layouts.module.scss";
import CartButton from "../../components/svg/CartButton";
import FafouriteButton from "../../components/svg/FavouriteButton";
// import CartButton from "../svg/StarButton"

const Header = () => {
    return (
        <header className={styles.header}>
            {/* <h1>Second Homework</h1> */}
            <img className={styles.siteLogo} src='./sashko-high-resolution-logo-black-transparent.png' alt="image" />

            <nav>

                <ul className={styles.navLinks}>

                    <li>
                        <button className={styles.menuButton}>Main</button>
                    </li>
                    <li>
                        <button className={styles.menuButton}>Products</button>
                    </li>
                    <li>
                        <button className={styles.menuButton}>About Us</button>
                    </li>
                    <li>
                        <button className={styles.menuButton}>Contacts</button>
                    </li>
                </ul>
            </nav>
            <div className={styles.svgContainer}>

                <div className={styles.svgButtons}>
                    <FafouriteButton className={styles.favouriteSvg} />
                    <span>1</span>

                </div>
                <div className={styles.svgButtons}>
                    <CartButton className={styles.cartSvg} />
                    <span>2</span>
                </div>
            </div>

            {/* <div className="cart-icon">
                <ShoppingCartIcon />
            </div> */}
        </header>
    );
};


export default Header