import PropTypes from 'prop-types'

import React from 'react';
import styles from "../Layouts.module.scss"

const Footer = () => {
    return (
      <footer className={styles.footer1}>
        <nav className={styles.footerNav}>     
                <ul className={styles.navLinks}>

                    <li>
                        <button className={styles.menuButton}>Support</button>
                    </li>
                    <li>
                        <button className={styles.menuButton}>Company</button>
                    </li>
                    <li>
                        <button className={styles.menuButton}>More info</button>
                    </li>
                    <li>
                        <button className={styles.menuButton}>Location</button>
                    </li>
                </ul>
            </nav>
        <p className= {styles.rights}>© 2024 My homework shop. All rights reserved.</p>
      </footer>
    );
  };

  export default Footer