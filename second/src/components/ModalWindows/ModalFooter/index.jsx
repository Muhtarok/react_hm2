import Button from "../../Buttons";
import PropTypes from "prop-types";

const ModalFooter = ({
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToFavBtn,
  thirdText,
  modalIsOpen,
  firstClick,
  secondaryClick, }) => {


  return (
    <div className="footer">
      {footerCancelBtn === "true" ? (
        <Button onClick={modalIsOpen} className="buttonPurple">{firstText}</Button>
      ) : (
        ""
      )}

      {footerDeleteBtn === "true" ? (
        <Button onClick={modalIsOpen} className="transparentBtn">{secondText}</Button>
      ) : (
        ""
      )}

      {footerAddToFavBtn === "true" ? (
        <Button onClick={modalIsOpen} className="buttonPurple">{thirdText}</Button>
      ) : (
        ""
      )}


    </div>

  )
}

ModalFooter.propTypes = {
  footerCancelBtn: PropTypes.oneOf(['true', 'false']).isRequired,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.oneOf(['true', 'false']).isRequired,
  secondText: PropTypes.string,
  footerAddToFavBtn: PropTypes.oneOf(['true', 'false']).isRequired,
  thirdText: PropTypes.string.isRequired,
  modalIsOpen: PropTypes.func.isRequired,
  firstClick: PropTypes.func.isRequired,
  secondaryClick: PropTypes.func.isRequired,
};




export default ModalFooter;
