import PropTypes from "prop-types";

const ModalBackground = ({modalIsOpen}) => {


  return (
    <div className="modalBackground" onClick={modalIsOpen}></div>
  )
}

ModalBackground.propTypes = {
  modalIsOpen: PropTypes.func.isRequired,
};

export default ModalBackground;
