import React, { useState, useEffect } from 'react';
import Card from '../Card';
// import productsData from '../../../public/posts.json';
import styles from "../Cards.module.scss"
import PropTypes from "prop-types";

function CardList({ products = [] }) {
  // const [products, setProducts] = useState([]);

  /*  useEffect(() => {
     // Simulating fetching products from JSON file
     setProducts(productsData);
   }, []); */

  console.log(products);

  return (
    <div className={styles.productList}>
      {products.map(({ name, price, imgWay, articul, color }) => (
        <Card
          key={products.articul}
          name={name}
          price={price}
          imgWay={imgWay}
          articul={articul}
          color={color}
        />
      ))}
    </div>
  );
}

export default CardList;
