import React from 'react';
import styles from "../Cards.module.scss"
import StarButton from "../svg/StarButton"
import PropTypes from "prop-types";
// import { ReactComponent as StarButton } from '../svg/StarButton';

function Card({ name, price, imgWay, articul, color }) {
  return (
    <div className={styles.productCard}>
      <h2 className={styles.title}>{name}</h2>
      <p className={styles.articul}>Articul: {articul}</p>
      <div className={styles.favouriteAdd}>
        <StarButton className="starSvg" />
      </div>
      <img className={styles.img} src={imgWay} alt={name} />
      

      <p className={styles.price}>Price: {price} USD.</p>
      <button type="button" className={styles.buttonAdd}>Add to Cart</button>
    </div>
  );
}

Card.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  imgWay: PropTypes.string,
  articul: PropTypes.number,
  color: PropTypes.string,
};

export default Card;


/* {
  "name": "Samsung S23",
  "price": 16000,
  "imgWay": "./3066117.jpeg",
  "articul": 1118,
  "color": "#ffffff"
}, */