import { useState } from "react";
import { useEffect, useImmer } from "react";
import Button from "./components/Buttons";
import ModalWrapper from "./components/ModalWindows/ModalWrapper";
import Header from "./Layouts/Header";
import Footer from "./Layouts/Footer";
import CardList from "./components/CardList/"
import axios from 'axios';



import "./App.css";
import "./App.scss";

function App() {
  const [isOpenFirstModal, setIsOpenFirstModal] = useState(false);
  const [isOpenSecondModal, setIsOpenSecondModal] = useState(false);

  const openFirstModalHandler = () => {
    setIsOpenFirstModal(!isOpenFirstModal);
  };

  const openSecondModalHandler = () => {
    setIsOpenSecondModal(!isOpenSecondModal);
  };

  const [products, setProducts] = useState([]);
  const [basket, setBasket] = useState([]);
  const [favourite, setFavourite] = useState([]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const { data } = await axios.get('./posts.json');
        setProducts(data);
        console.log(data);
      } catch (err) {
        console.log(err);
      }
    }

    fetchProducts();
  }, []);

  /*  const addFavourite = (articul) => {
     setFavourite([...favourite, articul]);
   };
 
   const addBasket = (articul) => {
     setBasket([...basket, articul]);
   }; */

  //()=>addFavourite("12323");

  /*  useEffect(function () {
     fetch("./posts.json").then(res => res.json()).then(data => setProducts(data))
     //use "filter" and remove for deleting;
     console.log(products);
   }, []) */







  return (
    <>
      <Header />
      <main>
        <div className="App">
          <h1>Good's List</h1>
          <CardList products={products}/>
        </div>
      </main>

      {/* <CardList /> */}
      {/* <div>
        {products && (
          products.map((el) => {
            return <div>
              {el.articul}
              <div>{el.imgWay}</div>
            </div>
          })
        )
        }
      </div>
 */}


      {/* <div className="buttonsContainer">
        <Button
          type="button"
          className="buttonPurple"
          onClick={openFirstModalHandler}
        >
          Open first modal
        </Button>

        <Button
          type="button"
          className="transparentBtn"
          onClick={openSecondModalHandler}
        >
          Open second modal
        </Button>
      </div> */}

      {isOpenFirstModal && (
        <ModalWrapper
          text="Product Delete"
          bodyText="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
          className="modal"
          hasImage="true"
          footerCancelBtn="true"
          firstText="NO, CANCEL"
          footerDeleteBtn="true"
          secondText="YES, DELETE"
          footerAddToFavBtn="false"
          thirdText=""
          modalIsOpen={openFirstModalHandler}
        />
      )}

      {isOpenSecondModal && (
        <ModalWrapper
          text="Add product 'NAME'"
          bodyText="Description for you product"
          className="modal"
          hasImage="false"
          footerCancelBtn="false"
          footerDeleteBtn="false"
          footerAddToFavBtn="true"
          thirdText="ADD TO FAVORITE"
          modalIsOpen={openSecondModalHandler}
        />
      )}

      <Footer />
    </>
  );
}

export default App;


/* import './App.scss'
import { useImmer } from 'use-immer'
import { useState, useEffect } from 'react';
import axios from 'axios'
import PostsContainer from './components/PostContainer';

const App = () => {
  const URL = 'https://ajax.test-danit.com/api/json/posts';

  const [posts, setPosts] = useImmer([]);
  const [isLoading, setIsLoading] = useState(false)

  const handleFavourite = (id) => {
    setPosts( draft => {
      const post = draft.find(post => id === post.id);
      post.isFavourite = !post.isFavourite;
    })
  } 

  async function fetchPosts() {
    setIsLoading(true)
    const { data } = await axios.get('./posts.json');
    setPosts(data)
    console.log(data)
  }

  useEffect(function () {
    try {

      fetchPosts()
    } catch (err) {
      console.error(err.message)

    } finally {
      setIsLoading(false)
    }

  }, [])

  return (
    <>
    <PostsContainer posts={posts} isLoading={isLoading} handleFavourite={handleFavourite}/>
    </>
  )
}



export default App; */


